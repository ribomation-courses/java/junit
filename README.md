JUnit, 1 day
====

Welcome to this course.
The syllabus can be found at
[java/junit](https://www.ribomation.se/courses/java/junit.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to demo projects

Installation Instructions
====

In order to do the programming exercises of the course, you need to have

Java JDK, version 8 or later installed. 
* [Java JDK Download](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

You also need a decent Java IDE. 
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download)

In addition, you need a GIT client to easily get the solutions and demo code of the course.
* [GIT Client Download](https://git-scm.com/downloads)

Finally, you need two build tools for some of the exercises
* [Maven](https://maven.apache.org/download.cgi)
* [Gradle](https://gradle.org/install/)

Usage of this GIT Repo
====

You need to have a GIT client installed to clone this repo. 
Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

Get the sources initially by a `git clone` operation. We recommend to create a top-level
directory for this course and two sub-directories; one for your own solutions and one for this GIT repo. 

Open a GIT BASH window and type
    
    mkdir -p ~/junit-course/my-solutions
    cd ~/junit-course
    git clone https://gitlab.com/ribomation-courses/java/java-basics.git gitlab    

Get the latest updates of this repo by a `git pull` operation

    cd ~/junit-course/gitlab
    git pull

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
