package ribomation.unit_tests;

public class StringUtils {

    public static String toTrainCase(String camelCase) {
        String result = "", word = "";
        for (int k = 0; k < camelCase.length(); ++k) {
            char ch = camelCase.charAt(k);
            if (Character.isUpperCase(ch)) {
                if (result.isEmpty()) {
                    result = word;
                } else {
                    result += "-" + word;
                }
                word = "" + Character.toUpperCase(ch);
            } else {
                if (k == 0) {
                    ch = Character.toUpperCase(ch);
                }
                word += ch;
            }
        }
        if (!word.isEmpty()) {
            result += "-" + word;
        }

        return result;
    }

}
