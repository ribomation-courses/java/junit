package ribomation.unit_tests;

import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.*;

public class StringUtilsTest {

    @Test
    @DisplayName("theMagicAnswer -> The-Magic-Answer")
    void magic_answer() {
        String in  = "theMagicAnswer";
        String out = "The-Magic-Answer";
        assertThat(StringUtils.toTrainCase(in))
                .isEqualTo(out)
        ;
    }

}
