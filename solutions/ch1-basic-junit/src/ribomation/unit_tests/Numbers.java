package ribomation.unit_tests;

public class Numbers {
    public static long fib(int n) {
        if (n < 0) throw new IllegalArgumentException("negative");
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fib(n - 2) + fib(n - 1);
    }
}
