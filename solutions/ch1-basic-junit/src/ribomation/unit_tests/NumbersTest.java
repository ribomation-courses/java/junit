package ribomation.unit_tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumbersTest {
    @Test
    @DisplayName("simple invocations of fib()")
    void simple_fib() {
        assertEquals(55, Numbers.fib(10));
    }
}
