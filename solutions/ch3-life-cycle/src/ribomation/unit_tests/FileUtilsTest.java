package ribomation.unit_tests;

import org.junit.jupiter.api.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FileUtilsTest {
    InputStream input;
    Path        output;
    static Path tmpDir;

    @BeforeAll
    static void createTmpDir() throws IOException {
        tmpDir = Files.createTempDirectory("unit-test");
    }

    @BeforeEach
    @DisplayName("opens the test data to IS")
    void load_data() {
        input = getClass().getResourceAsStream("/test-data.txt");
        String filename = "tmp-" + System.currentTimeMillis() + ".txt";
        output = Paths.get(tmpDir.toString(), filename);
    }

    @AfterEach
    @DisplayName("removes the copied file")
    void clean() throws IOException {
        Files.deleteIfExists(output);
    }

    @AfterAll
    static void removeTmpDir() throws IOException {
        Files.deleteIfExists(tmpDir);
    }

    @Test
    @DisplayName("null input, should throw")
    void null_should_throw() {
        assertThrows(IllegalArgumentException.class, () -> {
            FileUtils.copyFile(null, null);
        });
    }

    @Test
    @DisplayName("copy IS to path, should store a file in tmp dir")
    void copy_file() throws IOException {
        FileUtils.copyFile(input, output);

        assertTrue(Files.exists(output));
        List<String> lines = Files.readAllLines(output);
        assertEquals(lines.size(), 3);
    }

}
