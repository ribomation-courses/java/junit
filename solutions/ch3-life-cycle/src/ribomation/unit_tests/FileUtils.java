package ribomation.unit_tests;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class FileUtils {

    public static void copyFile(InputStream in, Path out) {
        if (in == null) {
            throw new IllegalArgumentException("input-stream was null");
        }
        if (out == null) {
            throw new IllegalArgumentException("output-stream was null");
        }

        try {
            Files.copy(in, out, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

}
