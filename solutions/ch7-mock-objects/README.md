Run with Gradle Wrapper
====

Linux
----
    ./gradlew test

Windows
----
    gradlew.bat test

Test Report
----

After running the test, the report can be found in

    ./build/reports/tests/test/index.html

