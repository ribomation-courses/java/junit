package ribomation.unit_tests;

import java.util.Map;

public class PersonDAO {
    static final String tbl = "persons";
    Database db;

    public PersonDAO(Database db) {
        this.db = db;
    }

    int numPersons() {
        return db.count(tbl);
    }

    Person find(String name) {
        String sql = String.format("SELECT * FROM %s WHERE name = '%s'",
                                   tbl, name);
        Map<String, String> row = db.firstRow(sql);
        if (row == null) throw new IllegalArgumentException("No such person name: " + name);

        name    = row.get("name");
        int age = Integer.parseInt(row.get("age"));
        return new Person(name, age);
    }

    Person insert(Person p) {
        String sql = String.format("INSERT INTO %s (name,age) VALUES ('%s',%d)",
                                   tbl, p.getName(), p.getAge());
        db.insert(sql);
        return p;
    }

    public void remove(Person p) {
        String sql = String.format("DELETE FROM %s WHERE name = '%s'", tbl, p.getName());
        db.remove(sql);
    }
}

