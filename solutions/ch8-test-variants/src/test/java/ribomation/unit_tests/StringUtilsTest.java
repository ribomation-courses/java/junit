package ribomation.unit_tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StringUtilsTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/emails-correct.txt")
    void correct_emails(String email){
        assertTrue(StringUtils.isEmail(email));
    }


    @ParameterizedTest
    @CsvFileSource(resources = "/emails-faulty.txt")
    void faulty_emails(String email){
        assertFalse(StringUtils.isEmail(email));
    }

}
