package ribomation.unit_tests;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ManyTestMethods {
    final int N = 30;

    @TestFactory
    Stream<DynamicTest> generate() {
        return IntStream.rangeClosed(1,N)
                .mapToObj(k ->
                        DynamicTest.dynamicTest("tst-"+k, () -> {
                            Thread.sleep(500);
                            assertTrue(k <= N);
                        }));
    }

}
