package ribomation.unit_tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TextUtilsTest {

    @Test
    void simple_test() {
        assertEquals("*****hello", TextUtils.rightPad("hello", 10, '*'));
    }

    @Test
    @DisplayName("if text do not fit, throw exception")
    void do_not_fit() {
        Exception x = assertThrows(IllegalArgumentException.class, () -> {
            TextUtils.rightPad("hello", 2, '*');
        });
    }

    @Test
    @DisplayName("passing null string, should return null")
    void null_argument() {
        assertNull(TextUtils.rightPad(null, 10, '*'));
    }

    @Test
    void simple_camel_case() {
        List<String> expected = Arrays.asList("unit", "testing", "is", "fun");
        List<String> actual   = TextUtils.splitCamelCase("unitTestingIsFun");
        assertIterableEquals(expected, actual);
    }
}
