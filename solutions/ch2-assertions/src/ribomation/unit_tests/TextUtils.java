package ribomation.unit_tests;

import java.util.ArrayList;
import java.util.List;

public class TextUtils {

    public static String rightPad(String txt, int width, char pad) {
        if (txt == null) {
            return null;
        }
        if (txt.length() > width) {
            throw new IllegalArgumentException();
        }

        int    padWidth = width - txt.length();
        String padding  = "";
        while (padWidth-- > 0) padding += pad;
        return padding + txt;
    }

    public static List<String> splitCamelCase(String camelCaseText) {
        // unitTestingIsFun
        List<String> words = new ArrayList<>();

//        for (String w : camelCaseText.split("[A-Z]")) {
//            words.add(w.toLowerCase());
//        }

        String word = "";
        for (int k = 0; k < camelCaseText.length(); ++k) {
            char ch = camelCaseText.charAt(k);
            if (Character.isUpperCase(ch)) {
                words.add(word);
                word = "" + Character.toLowerCase(ch);
            } else {
                word += ch;
            }
        }
        if (!word.isEmpty()) {
            words.add(word);
        }

        return words;
    }
}
