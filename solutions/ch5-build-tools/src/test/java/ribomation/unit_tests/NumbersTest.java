package ribomation.unit_tests;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Bunch of numbers tests")
public class NumbersTest {
    @Test
    @DisplayName("SUM(1..10) -> 55")
    void sum_should_pass() {
        assertEquals(55, Numbers.sum(10));
    }

    @Test
    @DisplayName("5! -> 120")
    void prod_should_pass() {
        assertEquals(120, Numbers.prod(5));
    }

    @Test
    @DisplayName("Fibonacci(10) -> 55")
    void simple_fib() {
        assertEquals(55, Numbers.fib(10));
    }

    @Test
    @DisplayName("Fibonacci(2) -> 1")
    void simple_fib2() {
        assertEquals(1, Numbers.fib(2));
    }

    @Test
    @DisplayName("SUM(1..10) =/= 42")
    @Disabled
    void failing_test() {
        assertEquals(42, Numbers.sum(10));
    }
}
