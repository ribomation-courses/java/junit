package ribomation.unit_tests;

public class Numbers {
    public static long sum(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;
        return n * (n + 1) / 2;
    }

    public static long prod(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;
        return n * prod(n - 1);
    }

    public static long fib(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;
        return fib(n - 2) + fib(n - 1);
    }
}
