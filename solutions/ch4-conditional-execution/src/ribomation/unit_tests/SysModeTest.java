package ribomation.unit_tests;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.*;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

public class SysModeTest {
    public static final String MODE_KEY = "ribomation.mode";

    @BeforeAll
    static void setMode() {
        List<String> modes = Arrays.asList("DEV", "QA", "PROD", "OTHER");
        Random       r = new Random(System.nanoTime());
        System.setProperty(MODE_KEY, modes.get(r.nextInt(modes.size())));
    }

    @Test
    @EnabledIfSystemProperty(named = MODE_KEY, matches = "DEV")
    @DisplayName("Run if DEV mode")
    void run_if_dev(){
        assertTrue(true);
    }

    @Test
    @EnabledIfSystemProperty(named = "ribomation.mode", matches = "QA")
    @DisplayName("Run if QA mode")
    void run_if_qa(){
        assertTrue(true);
    }

    @Test
    @EnabledIfSystemProperty(named = "ribomation.mode", matches = "PROD")
    @DisplayName("Run if PROD mode")
    void run_if_prod(){
        assertTrue(true);
    }

    @Test
    @DisabledIfSystemProperty(named = "ribomation.mode", matches = "(DEV|QA|PROD)")
    @DisplayName("Run otherwise")
    void run_if_otherwise(){
        assertTrue(true);
    }

}
